++ Report
- Introduction
- Heuristics & algorithms
- Implementation
- Statistical analysis
- Comparison with other algorithms
- Appendices with diagrams

++ Implementation 
+ Heuristics
- Total height
- Irregularity or average differences
- Monte Carlos
- Gradient descent