import java.io.*;
import java.time.Instant;
import java.util.Random;

public class PlayerSkeleton {
	GivenState s;
	PrintWriter logger;

	//implement this function to have a working system
	public PlayerSkeleton() {
		s = new GivenState();
	}
	public PlayerSkeleton(double[] probability) {
		s = new GivenState(probability);
	}
	public PlayerSkeleton(File file){
		s = new GivenState(file);
	}


	public int[] pickMove(GivenState s, int[][] legalMoves) {
		Random ran = new Random();
		int m = ran.nextInt(legalMoves.length);
		return legalMoves[m];
	}
	
	public void run() {
		new TFrame(s);
		initLogger();
		while(!s.hasLost()) {
			logger.println("=== TURN #" + s.getTurnNumber() + " ===");
			int moveArr[] = this.pickMove(s, s.legalMoves());
			logger.println("Piece type: " + s.nextPiece);
			logger.println(String.format("Making move orient #%d at slot #%d", moveArr[GivenState.ORIENT]
					,moveArr[GivenState.SLOT]));
			s.makeMove(moveArr);
			s.draw();
			s.drawNext(0,0);
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			logger.println("=== END TURN ===");
		}
		System.out.println("You have completed "+s.getRowsCleared()+" rows.");
		logger.flush();
	}

	private void initLogger() {
		String fname = Instant.now().toEpochMilli() + ".txt";
		File f = new File ("out" + File.separator + fname);
		try {
			// Output the result
			logger = new PrintWriter(new BufferedWriter(new FileWriter(f)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
